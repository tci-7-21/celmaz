import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase,ref, onValue } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
  apiKey: "AIzaSyADdxvVQO9rD8zb61ivr5QZmfCPfA_GG6U",
  authDomain: "proyectofinalmlaf.firebaseapp.com",
  projectId: "proyectofinalmlaf",
  storageBucket: "proyectofinalmlaf.appspot.com",
  messagingSenderId: "801459670812",
  appId: "1:801459670812:web:68c23f7ec61ccdef7834d9"
};


const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
/*--------------------------------------------------- */

window.addEventListener('DOMContentLoaded', (event) => {
  mostrarProductosHTML();
});

/*--------------------------------------------------- */

function mostrarProductosHTML() {
    const dbRef = ref(db, 'productos');
    const section = document.querySelector('.products-grid');
  
    onValue(dbRef, (snapshot) => {
      section.innerHTML = ''; // Limpiar contenido anterior
  
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
  
        const productDiv = document.createElement('div');
        productDiv.className = 'product';
  
        const productImage = document.createElement('img');
        productImage.src = data.UrlImg;
        productImage.alt = '';
        productDiv.appendChild(productImage);
  
        const productTitle = document.createElement('h3');
        productTitle.className = 'product-title';
        productTitle.textContent = data.Nombre;
        productDiv.appendChild(productTitle);
  
        const productPrice = document.createElement('p');
        productPrice.className = 'product-price';
        productPrice.textContent = `$${data.Precio}`;
        productDiv.appendChild(productPrice);
  
        const productStock = document.createElement('p');
        productStock.className = 'product-stock';
        productStock.textContent = `Disponible: ${data.Cantidad} unidades`;
        productDiv.appendChild(productStock);
  
        const addToCartBtn = document.createElement('button');
        addToCartBtn.className = 'add-to-cart';
        addToCartBtn.textContent = 'Agregar al carrito';
        productDiv.appendChild(addToCartBtn);
  
        section.appendChild(productDiv);
      });
    }, { onlyOnce: true });
  }
