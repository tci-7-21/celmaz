import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';
const firebaseConfig = {
    apiKey: "AIzaSyADdxvVQO9rD8zb61ivr5QZmfCPfA_GG6U",
    authDomain: "proyectofinalmlaf.firebaseapp.com",
    projectId: "proyectofinalmlaf",
    storageBucket: "proyectofinalmlaf.appspot.com",
    messagingSenderId: "801459670812",
    appId: "1:801459670812:web:68c23f7ec61ccdef7834d9"
};
const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);
/*---------------------------------------------------------*/
const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const signInButton = document.getElementById("button");
const errorMensaje = document.getElementById("errorMensaje");
/*---------------------------------------------------------*/
formulario.addEventListener("submit", (event) => {
  event.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {

      window.location.href = "/HTML/opciones.html";
    })
    .catch((error) => {

      console.log("Error al iniciar sesión:", error);

      errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});
/*---------------------------------------------------------*/

